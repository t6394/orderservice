using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.ServerResponse;
using Core.Validation;
using OrderService.Clients.CustomerClients;
using OrderService.Clients.MessageQueueClients;
using OrderService.Extensions;
using OrderService.Model;
using OrderService.Model.Dtos.Requests;
using OrderService.Model.Dtos.Requests.RequestValidations;
using OrderService.Model.Dtos.Responses;
using OrderService.Repositories.Interfaces;
using OrderService.Services.Interfaces;

namespace OrderService.Services
{
    public class OrdersService : IOrdersService
    {
        private readonly IOrderRepository _orderRepository;
        private readonly ICustomerClient _client;
        private readonly IMessageQueueClient _messageQueueClient;

        public OrdersService(IOrderRepository orderRepository, ICustomerClient client, IMessageQueueClient messageQueueClient)
        {
            _orderRepository = orderRepository;
            _client = client;
            _messageQueueClient = messageQueueClient;
        }

        public async Task<Response<bool>> ChangeStatus(Guid id, ChangeStatusOrderRequest request)
        {
            ValidationTool.Validate(new ChangeStatusOrderRequestValidator(),request);

            var result = await _orderRepository.ChangeStatus(id, request.Status);
            if (result)
                return new SuccessResponse<bool>(result);
            return new ErrorResponse<bool>(result);
   
        }

        public async Task<Response<Guid>> Create(CreateOrderRequest request)
        {
            ValidationTool.Validate(new CreateOrderRequestValidator(),request);

            var customer = await _client.CustomerIsValid(request.CustomerId);
            if (customer.Success == false)
                return new ErrorResponse<Guid>(ResponseStatus.NotFound, default, ResultMessage.NotFoundCustomer);

            var order = request.ConvertToOrder();

            var result = await _orderRepository.Create(order);
            _messageQueueClient.Publish<Order>(RabbitMQHelper.CreatedQueue,order);
            return new SuccessResponse<Guid>(ResponseStatus.Created, result);
    
        }

        public async Task<Response<bool>> CreateMany(List<CreateOrderRequest> requests)
        {
            foreach(var request in requests){
                ValidationTool.Validate(new CreateOrderRequestValidator(),request);

                var customer = await _client.CustomerIsValid(request.CustomerId);
                if (customer.Success == false)
                return new ErrorResponse<bool>(ResponseStatus.NotFound, default, ResultMessage.NotFoundCustomer);
            }
            var orders = requests.ConvertToOrderList();
            var result = await _orderRepository.CreateMany(orders);
            return new SuccessResponse<bool>(result);
        }


        public async Task<Response<bool>> Delete(Guid id)
        {
            var result = await _orderRepository.Delete(id);
            if(result)
                return new SuccessResponse<bool>(result);
            return new ErrorResponse<bool>(result);
        }

        public async Task<Response<IEnumerable<OrderResponse>>> GetAll()
        {
            var orders = await _orderRepository.GetAll();
            return new SuccessResponse<IEnumerable<OrderResponse>>(orders.ConvertToOrderListResponse());
        }

        public async Task<Response<IEnumerable<OrderResponse>>> GetAllBySoftDeleted()
        {
            var orders = await _orderRepository.GetAllBySoftDeleted();
            return new SuccessResponse<IEnumerable<OrderResponse>>(orders.ConvertToOrderListResponse());
        }

        public async Task<Response<IEnumerable<OrderResponse>>> GetMany(List<Guid> ids)
        {
            var orders = await _orderRepository.GetMany(ids);
            return new SuccessResponse<IEnumerable<OrderResponse>>(orders.ConvertToOrderListResponse());
        }

        public async Task<Response<OrderResponse>> GetById(Guid id)
        {
            var order = await _orderRepository.Get(id);
            if(order is not null)
                return new SuccessResponse<OrderResponse>(order.ConvertToOrderResponse());
            return new ErrorResponse<OrderResponse>(ResponseStatus.NotFound, default,ResultMessage.NotFoundOrder);
        }

        public async Task<Response<bool>> SoftDelete(Guid id)
        {
            var result = await _orderRepository.SoftDelete(id);
            if(result)
                return new SuccessResponse<bool>(result);
            return new ErrorResponse<bool>(result);
        }

        public async Task<Response<bool>> Update(Guid id, UpdateOrderRequest request)
        {
            ValidationTool.Validate(new UpdateOrderRequestValidator(), request);

            var customer = await _client.CustomerIsValid(request.CustomerId);
            if (customer.Success == false)
                return new ErrorResponse<bool>(ResponseStatus.NotFound, default, ResultMessage.NotFoundCustomer);

            var order = request.ConvertToOrder(id);

            var result = await _orderRepository.Update(id, order);
            _messageQueueClient.Publish<Order>(RabbitMQHelper.UpdatedQueue,order);
            return new SuccessResponse<bool>(result);

        }

        public async Task<Response<OrderPaginationResponse>> Page(int from, int size)
        {
            var orders = await _orderRepository.Page(from, size);
            var totalOrderCount = await _orderRepository.TotalCountOfOrder();
            return new SuccessResponse<OrderPaginationResponse>(orders.CovertToPaginationOrderResponse(from,size,totalOrderCount));
        }
    }
}