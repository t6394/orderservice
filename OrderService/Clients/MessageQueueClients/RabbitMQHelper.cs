﻿namespace OrderService.Clients.MessageQueueClients
{
    public class RabbitMQHelper
    {
        public static string CreatedQueue => "Created-Order-Queue";
        public static string UpdatedQueue => "Updated-Order-Queue";
        public static string OrderExchange => "Order-Exchange";

    }
}