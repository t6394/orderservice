﻿using System;
using System.Threading.Tasks;
using Core.ServerResponse;

namespace OrderService.Clients.CustomerClients
{
    public interface ICustomerClient
    {
        Task<Response<bool>> CustomerIsValid(Guid id);
    }
}