﻿using System.Threading.Tasks;
using Core.ServerResponse;
using OrderService.Model.Dtos.Responses;

namespace OrderService.Clients.AuthClients
{
    public interface IAuthClient
    {
        Task<Response<TokenHandlerResponse>> TokenValidate(string token);
    }
}