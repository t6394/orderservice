using System.Collections.Generic;

namespace OrderService.Model.Dtos.Responses
{
    public class OrderPaginationResponse
    {
        public List<OrderResponse> OrderResponses { get; set; }
        public int From { get; set; }
        public int Size { get; set; }
        public long TotalItemCount { get; set; }
        public int CurrentItemCount { get; set; }
    }
}