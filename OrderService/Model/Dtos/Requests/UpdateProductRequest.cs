using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderService.Model.Dtos.Requests
{
    public class UpdateProductRequest
    {
        public string ImageUrl { get; set; }
        public string Name { get; set; }
    }
}