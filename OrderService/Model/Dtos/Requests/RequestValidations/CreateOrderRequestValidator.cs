using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;

namespace OrderService.Model.Dtos.Requests.RequestValidations
{
    public class CreateOrderRequestValidator : AbstractValidator<CreateOrderRequest>
    {
        public CreateOrderRequestValidator()
        {
            RuleFor(r => r.CustomerId).NotEmpty();
            RuleFor(r => r.Price).GreaterThan(0).NotEmpty();
            RuleFor(r => r.Quantity).GreaterThan(0).NotEmpty();

            RuleFor(r => r.Product.Name).MinimumLength(2).MaximumLength(150).NotEmpty();
            RuleFor(r => r.Product.ImageUrl).MinimumLength(2).MaximumLength(150).NotEmpty();

            RuleFor(r => r.Address.AddressLine).MinimumLength(2).MaximumLength(150).NotEmpty();
            RuleFor(r => r.Address.City).MinimumLength(2).MaximumLength(150).NotEmpty();
            RuleFor(r => r.Address.Country).MinimumLength(2).MaximumLength(150).NotEmpty();
            RuleFor(r => r.Address.CityCode).GreaterThan(0).NotEmpty();

        }
    }
}