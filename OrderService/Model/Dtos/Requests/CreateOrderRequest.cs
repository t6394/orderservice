using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderService.Model.Dtos.Requests
{
    public class CreateOrderRequest
    {
        public Guid CustomerId { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
        public CreateProductRequest Product { get; set; }
        public CreateAddressRequest Address { get; set; }
    }
}