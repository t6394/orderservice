using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderService.Model.Dtos.Requests
{
    public class UpdateOrderRequest
    {
        public Guid CustomerId { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
        public string Status { get; set; }
        public bool isDeleted { get; set; }
        public UpdateProductRequest Product { get; set; }
    }
}