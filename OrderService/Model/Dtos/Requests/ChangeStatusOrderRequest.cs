using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderService.Model.Dtos.Requests
{
    public class ChangeStatusOrderRequest
    {
        public string Status { get; set; }
    }
}