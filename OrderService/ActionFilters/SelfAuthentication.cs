﻿using System;
using System.Threading.Tasks;
using Core.ServerResponse;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using OrderService.Clients.AuthClients;
using OrderService.Model.Dtos.Requests;


namespace OrderService.ActionFilters
{
    public class SelfAuthenticationAttribute : TypeFilterAttribute
    {
        public SelfAuthenticationAttribute() : base(typeof(SelfAuthentication))
        {

        }
    }

    public class SelfAuthentication : IAsyncActionFilter
    {
        private readonly IAuthClient _authClient;

        public SelfAuthentication(IAuthClient authClient)
        {
            _authClient = authClient;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var requestBody = context.ActionArguments["request"] as CreateOrderRequest;
            Console.WriteLine(requestBody?.CustomerId);
            if (requestBody?.CustomerId != null)
            {
                var token = context.HttpContext.Request.Headers["Authorization"].ToString();
                if (String.IsNullOrEmpty(token) || !token.StartsWith("Bearer "))
                {
                    context.Result = new UnauthorizedObjectResult(new ErrorResponse(ResponseStatus.UnAuthorized,"Token not found."));
                    return;
                }
                var tokenIsValid = await _authClient.TokenValidate(token.Substring(7));
                if (tokenIsValid.Success == false || !tokenIsValid.Data.Id.Contains(requestBody.CustomerId.ToString()))
                {
                    context.Result = new UnauthorizedObjectResult(new ErrorResponse(ResponseStatus.UnAuthorized,"Invalid token"));
                    return;
                }
            }
            
            await next();
        }

    }
}