﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Core.ServerResponse;
using Microsoft.AspNetCore.Http;
using Microsoft.IO;
using OrderService.Clients.AuthClients;


namespace OrderService.Middleware
{
    public class TokenHandlerMiddleware
    {
        private readonly IAuthClient _authClient;
        private readonly RequestDelegate _next;
        private readonly RecyclableMemoryStreamManager _recyclableMemoryStreamManager;

        public TokenHandlerMiddleware(RequestDelegate next, IAuthClient authClient)
        {
            _next = next;
            _authClient = authClient;
            _recyclableMemoryStreamManager = new RecyclableMemoryStreamManager();
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                var requestBody = await GetRequestBody(context);

                if (requestBody.Contains("customerId"))
                {
                    var token = context.Request.Headers["Authorization"].ToString();
                    var tokenIsValid = await _authClient.TokenValidate(token.Substring(7));
                    if (!tokenIsValid.Data.Status)
                    {
                        throw new UnauthorizedAccessException("Invalid token");
                    }
                    if (!requestBody.Contains(tokenIsValid.Data.Id))
                    {
                        throw new UnauthorizedAccessException();
                    }
                }
                await _next(context);
                
            }
            catch (Exception e)
            {
                var response = context.Response;
                response.ContentType = "application/json";
                response.StatusCode = (int)HttpStatusCode.Unauthorized;

                var resp = new ErrorResponse(ResponseStatus.UnAuthorized,e.Message);
                var result = JsonSerializer.Serialize(resp);

                await response.WriteAsync(result);

            }
        }

        private static string ReadStreamInChunks(Stream stream)
        {
            const int readChunkBufferLength = 4096;

            stream.Seek(0, SeekOrigin.Begin);

            using var textWriter = new StringWriter();
            using var reader = new StreamReader(stream, Encoding.UTF8);

            var readChunk = new char[readChunkBufferLength];
            int readChunkLength;

            do
            {
                readChunkLength = reader.ReadBlock(readChunk,
                    0,
                    readChunkBufferLength);
                textWriter.Write(readChunk, 0, readChunkLength);

            } while (readChunkLength > 0);

            return textWriter.ToString();
        }

        private async Task<string> GetRequestBody(HttpContext context)
        {
            context.Request.EnableBuffering();

            await using var requestStream = _recyclableMemoryStreamManager.GetStream();
            await context.Request.Body.CopyToAsync(requestStream);

            string reqBody = ReadStreamInChunks(requestStream);

            context.Request.Body.Seek(0, SeekOrigin.Begin);

            return reqBody;
        }
    }
}