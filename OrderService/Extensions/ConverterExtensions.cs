using System;
using System.Collections.Generic;
using System.Linq;
using Core.Model;
using OrderService.Model;
using OrderService.Model.Dtos.Requests;
using OrderService.Model.Dtos.Responses;

namespace OrderService.Extensions
{
    public static class ConverterExtensions
    {
        public static Order ConvertToOrder(this CreateOrderRequest request){
            return new Order
                {
                    CustomerId = request.CustomerId,
                    Price = request.Price,
                    Quantity = request.Quantity,
                    Product = new Product {
                        Id = request.Product.Id,
                        Name = request.Product.Name,
                        ImageUrl = request.Product.ImageUrl
                    },
                    Address = new Address {
                        AddressLine = request.Address.AddressLine,
                        City = request.Address.City,
                        Country = request.Address.Country,
                        CityCode = request.Address.CityCode
                    }
                };
        }

        public static List<Order> ConvertToOrderList(this List<CreateOrderRequest> requests){
            List<Order> orders = new List<Order>();
            requests.ToList().ForEach(o => orders.Add(
                new Order
                    {
                        Quantity = o.Quantity,
                        Price = o.Price,
                        CustomerId = o.CustomerId,
                        Product = new Product 
                            {
                                Id = o.Product.Id,
                                ImageUrl = o.Product.ImageUrl,
                                Name = o.Product.Name
                            },
                        Address = new Address
                            {
                                AddressLine = o.Address.AddressLine,
                                City = o.Address.City,
                                Country = o.Address.Country,
                                CityCode = o.Address.CityCode,
                            }
                    }
            ));
            return orders;
        }


        public static IEnumerable<OrderResponse> ConvertToOrderListResponse(this IEnumerable<Order> orders){
            List<OrderResponse> orderResponses = new List<OrderResponse>();
            orders.ToList().ForEach(o => orderResponses.Add(
                new OrderResponse
                    {
                        Id = o.Id,
                        CustomerId = o.CustomerId,
                        Quantity = o.Quantity,
                        Price = o.Price,
                        Status = o.Status,
                        CreatedAt = o.CreatedAt,
                        UpdatedAt = o.UpdatedAt,
                        isDeleted = o.isDeleted,
                        Address = new AddressResponse 
                            {
                                AddressLine = o.Address.AddressLine,
                                City = o.Address.City,
                                Country = o.Address.Country,
                                CityCode = o.Address.CityCode
                            },
                        Product = new ProductResponse
                            {
                                Id = o.Product.Id,
                                ImageUrl = o.Product.ImageUrl,
                                Name = o.Product.Name
                            }
                    }
            ));
            return orderResponses;
        }

        public static OrderResponse ConvertToOrderResponse(this Order order){
            return new OrderResponse
            {
                Id = order.Id,
                CustomerId = order.CustomerId,
                Quantity = order.Quantity,
                Price = order.Price,
                Status = order.Status,
                CreatedAt = order.CreatedAt,
                UpdatedAt = order.UpdatedAt,
                isDeleted = order.isDeleted,
                Address = new AddressResponse
                {
                    AddressLine = order.Address.AddressLine,
                    City = order.Address.City,
                    Country = order.Address.Country,
                    CityCode = order.Address.CityCode
                },
                Product = new ProductResponse
                {
                    Id = order.Product.Id,
                    ImageUrl = order.Product.ImageUrl,
                    Name = order.Product.Name
                }
            };
        }


        public static Order ConvertToOrder(this UpdateOrderRequest request, Guid id){
            return new Order
                {
                    Id = id,
                    CustomerId = request.CustomerId,
                    Quantity = request.Quantity,
                    Price = request.Price,
                    isDeleted = request.isDeleted,
                    Product = new Product
                        {
                            ImageUrl = request.Product.ImageUrl,
                            Name = request.Product.Name
                        }
                };
        }

        public static Address ConvertToAddress(this AddressResponse address){
            return new Address
                {
                    AddressLine = address.AddressLine,
                    City = address.City,
                    Country = address.Country,
                    CityCode = address.CityCode
                };
        }


        public static OrderPaginationResponse CovertToPaginationOrderResponse(this IEnumerable<Order> orders, int from, int size, long totalOrderCount){
            List<OrderResponse> orderResponses = new List<OrderResponse>();
            orders.ToList().ForEach(o => orderResponses.Add(
                new OrderResponse
                    {
                    Id = o.Id,
                    CustomerId = o.CustomerId,
                    Quantity = o.Quantity,
                    Price = o.Price,
                    Status = o.Status,
                    CreatedAt = o.CreatedAt,
                    UpdatedAt = o.UpdatedAt,
                    isDeleted = o.isDeleted,
                    Address = new AddressResponse
                    {
                        AddressLine = o.Address.AddressLine,
                        City = o.Address.City,
                        Country = o.Address.Country,
                        CityCode = o.Address.CityCode
                    },
                    Product = new ProductResponse
                    {
                        Id = o.Product.Id,
                        ImageUrl = o.Product.ImageUrl,
                        Name = o.Product.Name
                    }
                    }
            ));
            return new OrderPaginationResponse
                {
                    OrderResponses = orderResponses,
                    From = from,
                    Size = size,
                    TotalItemCount = totalOrderCount,
                    CurrentItemCount = orders.Count()
                };
        }

    }
}