﻿using Core.Repositories.Settings;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using OrderService.Clients.AuthClients;
using OrderService.Clients.CustomerClients;
using OrderService.Clients.MessageQueueClients;
using OrderService.Repositories;
using OrderService.Repositories.Interfaces;
using OrderService.Services;
using OrderService.Services.Interfaces;

namespace OrderService.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddRepositories(this IServiceCollection services, IConfiguration configuration)
        {
            BsonSerializer.RegisterSerializer(new GuidSerializer(MongoDB.Bson.BsonType.String));
            BsonSerializer.RegisterSerializer(new DateTimeOffsetSerializer(MongoDB.Bson.BsonType.String));

            services.Configure<MongoSettings>(configuration.GetSection(nameof(MongoSettings)));
            services.AddSingleton<IMongoSettings>(d=>d.GetRequiredService<IOptions<MongoSettings>>().Value);
            
            services.AddSingleton<IOrderRepository, OrderRepository>();
            return services;
        }
        
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddSingleton<IOrdersService, OrdersService>();
            return services;
        }
        
        public static IServiceCollection AddClients(this IServiceCollection services)
        {
            services.AddSingleton<ICustomerClient,CustomerClient>();
            services.AddSingleton<IAuthClient,AuthClient>();
            services.AddHttpClient();
            return services;
        }

        public static IServiceCollection AddRabbit(this IServiceCollection services)
        {
            services.AddSingleton<IMessageQueueClient, RabbitMQClient>();
            return services;
        }
        
        public static IServiceCollection AddUtilities(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "OrderService", Version = "v1" });
            });

            services.AddCors(options =>
            {
                options.AddPolicy("AllowMyOrigin", builder =>
                    builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader());
            });
            
            return services;
        }
        
    }
}